<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license 
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace App;
use Routes\Router;
use Routes\Middleware;
use Logging\BaseLogger;

require __DIR__ . '/../vendor/autoload.php';

$config = \Config\Config::get('database');
$Application = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
        'db' => [
            'driver' => $config['driver'],
            'host' => $config['host'],
            'database' => $config['database'],
            'username' => $config['username'],
            'password' => $config['password'],
            'charset'   => $config['charset'],
            'collation' => $config['collation'],
            'prefix'    => $config['prefix'],
            'options'   => [
                \PDO::ATTR_EMULATE_PREPARES => true
            ]
        ]
    ]
]);
$container = $Application->getContainer();
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$container['db'] = function ($container) use ($capsule) {
    return $capsule;
};
$container['errorHandler'] = function ($container) {
    return function ($request, $response, $exception) use ($container) {
        if(method_exists($exception, 'getResponse')) {
            $data = $exception->getResponse();
            $response = $response->withStatus($exception->getCode());
            BaseLogger::call($request, $response, false);
        } else {
            BaseLogger::log($exception->getMessage(), 'NEW EXCEPTION!', \Config\Config::get('discordChannel', 'error'), BaseLogger::ERROR);
            $data = $exception->getMessage();
        }
        $code = $exception->getCode() ? $exception->getCode() : 500;
        return $container->get('response')->withStatus($code)->withJson($data);
    };
};

Router::init($Application);
Middleware::init($Application);
