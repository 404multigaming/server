<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Config;

/**
 * Class Config
 * @package Config
 */
class Config {

    /**
     * @var array
     */
    private $config;

    /**
     * @var Config
     */
    private static $instance;

    /**
     * Config constructor.
     */
    public function __construct() {
        $files = scandir(__DIR__);
        $result = [];
        foreach ($files as $file) {
            $file_parts = pathinfo($file);
            if ($file_parts['extension'] === 'ini') {
                if (($this->isProd() && strpos($file, '_dev')) || (!$this->isProd() && strpos($file, '_prod'))) {
                    continue;
                }
                $content = parse_ini_file($file, true);
                $result = array_merge($result, $content);
            }
        }
        $this->config = $result;
    }

    /**
     * @return bool
     */
    public function isProd() : bool {
       $hostname = $_SERVER['HTTP_HOST'];
       if($hostname === 'asdsd.de') {
           return true;
       }
       ini_set("display_errors", 1);
       return false;
   }

    /**
     * @param string $category
     * @param string $key
     *
     * @return string|array
     */
    public static function get(string $category, string $key = null) {
        if(self::$instance === null) {
            self::$instance = new Config;
        }
        if($key === null) {
            return self::$instance->config[$category];
        }
        return self::$instance->config[$category][$key];
    }
}
