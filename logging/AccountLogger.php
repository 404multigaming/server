<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Logging;

use Config\Config;
use Slim\Http\Request;
use Slim\Http\Response;
use Server\Models\Database\Account\User;
use Logging\BaseLogger;

 /**
  * Class BaseLogger
  * @package Logging
  */
 class AccountLogger extends BaseLogger {

     /**
      * Returns the discord account logging channel
      *
      * @return int
      */
     private function channel() : int {
         return Config::get('discordChannel', 'account');
     }

     /**
      * @param Request  $req
      * @param User     $user
      */
     public function registration(Request $req, User $user) {
         $message = "**Message:** {$user->firstname} ({$user->username}) has created a new account.\n**IP:** {$req->getServerParam('REMOTE_ADDR')}";
         $title = 'New registration';
         self::log($message, $title, self::channel(), static::SUCCESSFULL);
     }

     /**
      * @param Request  $req
      * @param string   $username
      */
     public function invalidLogin(Request $req, string $username) {
         $message = "**Message:** Someone attempted to login into the Account of {$username} with an invalid password/username.\n**IP:** {$req->getServerParam('REMOTE_ADDR')}";
         $title = 'Invalid login attempt!';
         self::log($message, $title, self::channel(), static::WARNING);
     }

 }
