<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Logging;

use Config\Config;
use RestCord\DiscordClient;
use Logging\DiscordRateLimitProvider;
use Server\Cache\Cache;
use Slim\Http\Request;
use Slim\Http\Response;

 /**
  * Class BaseLogger
  * @package Logging
  */
 class BaseLogger {

     const INFO = 11393254;
     const WARNING = 16744192;
     const ERROR = 16711680;
     const SUCCESSFULL = 65280;

     /**
      * @return DiscordClient;
      */
     protected function API() {
          return new DiscordClient(['token' => Config::get('discord', 'apikey'), 'rateLimitProvider' => new DiscordRateLimitProvider]);
     }

     /**
      * @param Request  $req
      * @param Response $res
      * @param bool     $cache
      */
     public function call(Request $req, Response $res, bool $cache) {
         $message = "**URL:** {$req->getUri()->getPath()}\n**Code:** {$res->getStatusCode()}\n**IP:** {$req->getServerParam('REMOTE_ADDR')}";
         $message .= "\n**Cache:** " . ($cache ? 'true' : 'false');             
         $title = $req->getUri()->getPath();
         if ($res->getStatusCode() < 300) {
             $type = static::SUCCESSFULL;
         } else if ($res->getStatusCode() >= 300 && $res->getStatusCode() < 500) {
             $type = static::WARNING;
         } else if ($res->getStatusCode() >= 500 ) {
             $type = static::ERROR;
         }
         self::log($message, $title, Config::get('discordChannel', 'apiCall'), $type);
     }

     /**
      * @param string   $message
      * @param string   $title
      * @param int      $channel
      * @param int      $type
      */
     public function log(string $message, string $title, int $channel, int $type) {
         self::create($message, $title, $channel, $type);
     }

     /**
      * @param string   $message
      * @param string   $title
      * @param int      $channel
      * @param int      $type
      */
     private function create(string $message, string $title, int $channel, int $type) {

         //Build author
         $author['name'] = Config::get('discord', 'authorName');
         $author['icon_url'] = Config::get('discord', 'authorIcon');

         //Build Embed
         $param['color'] = $type;
         $param['title'] = $title . (Config::isProd() ? null : ' (Testsystem)');
         $param['description'] = $message;
         $param['author'] = $author;

         //Build Entry
         $entry['channel.id'] = $channel;
         $entry['embed'] = $param;
         self::API()->channel->createMessage($entry);
     }


 }
