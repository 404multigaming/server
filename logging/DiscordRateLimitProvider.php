<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Logging;

use Server\Cache\Cache;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use RestCord\RateLimit\Provider\AbstractRateLimitProvider;

 /**
  * Class DiscordRateLimitProvider
  * @package Logging
  */
 class DiscordRateLimitProvider extends AbstractRateLimitProvider {

     const MAX_TTL = 60 * 60 * 24 * 7;
     /**
      * Returns the prefixed version of the key.
      *
      * @param string $key
      *
      * @return string
      */
     public function getKey($key) : string {
         return "restcord.ratelimit.{$key}";
     }

     /**
      * Returns when the last request was made.
      *
      * @param RequestInterface $request
      *
      * @return float|null When the last request was made.
      */
     public function getLastRequestTime(RequestInterface $request) {
         $route = $this->getRoute($request);
         $key   = $this->getKey("{$route}.lastRequest");

         return Cache::get($key);
     }

     /**
      * Used to set the current time as the last request time to be queried when
      * the next request is attempted.
      *
      * @param RequestInterface $request
      */
     public function setLastRequestTime(RequestInterface $request) {
         $route = $this->getRoute($request);
         $key   = $this->getKey("{$route}.lastRequest");

         Cache::set($key, $this->getRequestTime($request), static::MAX_TTL);
     }

     /**
      * Returns the minimum amount of time that is required to have passed since
      * the last request was made. This value is used to determine if the current
      * request should be delayed, based on when the last request was made.
      *
      * Returns the allowed  between the last request and the next, which
      * is used to determine if a request should be delayed and by how much.
      *
      * @param RequestInterface $request The pending request.
      *
      * @return float The minimum amount of time that is required to have passed
      *               since the last request was made (in microseconds).
      */
     public function getRequestAllowance(RequestInterface $request) : float {
         $route = $this->getRoute($request);
         $key   = $this->getKey("{$route}.reset");
         $entry = Cache::get($key);
         if($entry === null) {
             return 0;
         }

         return ($entry - time()) * 1000000;
     }

     /**
      * Used to set the minimum amount of time that is required to pass between
      * this request and the next (in microseconds).
      *
      * @param RequestInterface  $request
      * @param ResponseInterface $response The resolved response.
      */
     public function setRequestAllowance(RequestInterface $request, ResponseInterface $response) {
         $route = $this->getRoute($request);

         $remaining = $response->getHeader('X-RateLimit-Remaining');
         $reset     = $response->getHeader('X-RateLimit-Reset');
         if (empty($remaining) || empty($reset) || (int) $remaining[0] > 0) {
             return;
         }

         Cache::set($this->getKey("{$route}.reset"), $reset[0], static::MAX_TTL);
     }
 }
