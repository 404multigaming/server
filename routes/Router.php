<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Routes;

use \Slim\App;
use Config\Config;
use Server\Controller\System\RequestController;
use Slim\Http\Request;
use Slim\Http\Response;
use Logging\BaseLogger;

 /**
  * Class Router
  * @package Routes
  */
 class Router {

     /**
      * @param App $app
      */
     public function init(App $app) {
         $version = Config::get('system', 'version');
         $app->get('/favicon.ico', function (Request $req, Response $res) {
            return $res->withStatus(204);
         });
         self::serverRoutes($version, $app);
         self::riotRoutes($version, $app);
         self::accountRoutes($version, $app);
     }

    /**
    * @param string   $version
    * @param App      $app
    */
    private function accountRoutes(string $version, App $app) {
        $app->group("/v{$version}/account", function() {

            $this->post('/create', 'Server\Controller\Account\AccountController:create');
            $this->post('/login', 'Server\Controller\Account\AuthenticationController:login');
            $this->get('/delete', 'Server\Controller\Account\AccountController:delete');
            $this->get('', 'Server\Controller\Account\AccountController:current');

        })->add(function (Request $req, Response $res, callable $next) {
            return self::cacheCall($req, $res, $next);
        });
    }

    /**
    * @param string   $version
    * @param App      $app
    */
    private function riotRoutes(string $version, App $app) {
        $app->group("/v{$version}/riot", function() {

            $this->get('/summoner/{summoner}', 'Server\Controller\Riot\SummonerController:getLeagueByName');

        })->add(function (Request $req, Response $res, callable $next) {
            return self::cacheCall($req, $res, $next);
        });
    }

    /**
    * @param string   $version
    * @param App      $app
    */
    private function serverRoutes(string $version, App $app) {
        $app->group("/v{$version}/server", function() {
            $this->get('/ping', 'Server\Controller\System\SystemController:ping');
            $this->post('/deploy', 'Server\Controller\System\SystemController:deploy');
        });
    }

     /**
      * @param Request $req
      * @param Response $res
      * @param callable $next
      *
      * @return Response
      * @throws RateLimitException
      * @throws \Server\Models\Exceptions\RateLimitException
      */
     private function cacheCall(Request $req, Response $res, callable $next) : Response {
         $ratelimit = RequestController::rateLimit($req);
         $response = RequestController::isCached($req, $res);
         if($response === null) {
             $response = $next($req, $res);
             $response = $response->withHeader('cache', 'false');
             RequestController::cacheCall($req, $response);
             BaseLogger::call($req, $response, false);
         } else {
             $response = $response->withHeader('cache', 'true');
             BaseLogger::call($req, $response, true);
         }
         $response = $response->withHeader('Rate-Limit', $ratelimit);
         return $response;
     }

 }
