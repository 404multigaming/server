<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Routes;

use \Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

/**
  * Class Middleware
  * @package Routes
  */
 class Middleware {

     /**
      * @param App $app
      */
     public function init(App $app) {
         $app->add(function (Request $req, Response $res, callable $next) {
             $response = $next($req, $res);
             return $response
                   ->withHeader('Access-Control-Allow-Origin', '*')
                   ->withHeader('Access-Control-Allow-Headers', 'Content-Type, Accept, Origin, Auth')
                   ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
                   ->withHeader('Content-Type', 'application/json');
         });
     }
 }
