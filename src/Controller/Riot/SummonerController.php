<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Controller\Riot;

use Slim\Http\Request;
use Slim\Http\Response;
use Server\Controller\Controller;
use Server\Library\Riot\RiotAPI;
use Server\Models\Exceptions\BaseException;

 /**
  * Class SummonerController
  * @package Server\Controller\Riot
  */
 class SummonerController extends Controller {

     /**
      * @param Request $req
      * @param Response $res
      *
      * @param array $params
      * @return Response
      * @throws BaseException
      * @throws \RiotAPI\LeagueAPI\Exceptions\GeneralException
      * @throws \RiotAPI\LeagueAPI\Exceptions\RequestException
      * @throws \RiotAPI\LeagueAPI\Exceptions\ServerException
      * @throws \RiotAPI\LeagueAPI\Exceptions\ServerLimitException
      * @throws \RiotAPI\LeagueAPI\Exceptions\SettingsException
      */
     public function getLeagueByName(Request $req, Response $res, array $params) {
         $summoner = $params['summoner'];
         if($summoner === null) {
             throw new BaseException('The given email is already in use or invalid.', 400, 'invalidEmail');
         }
         $summoner = RiotAPI::api()->getSummonerByName($summoner);
         $league = RiotAPI::api()->getLeaguePositionsForSummoner($summoner->id);        
         return self::successfull($res, (array)$league);
     }
 }
