<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Controller\System;

use Slim\Http\Request;
use Slim\Http\Response;
use Server\Controller\Controller;
use Server\Models\Exceptions\UnauthorizedException;
use Config\Config;

 /**
  * Class SystemController
  * @package Server\Controller\System
  */
 class SystemController extends Controller {

     /**
      * @param Request $req
      * @param Response $res
      *
      * @return Response
      */
     public function ping(Request $req, Response $res) {
         return self::successfull($res);
     }

     /**
      * @param Request $req
      * @param Response $res
      *
      * @return Response
      * @throws \Server\Models\Exceptions\InvalidBodyException
      * @throws UnauthorizedException
      * @throws \Server\Models\Exceptions\InvalidBodyException
      */
     public function deploy(Request $req, Response $res) {
         if(self::header($req, 'X-Event-Key') !== 'repo:push') {
             throw new UnauthorizedException;
         }
         $payload = json_decode(self::post($req, 'push'));
         foreach ($payload->changes as $commit) {
             $branch = $commit->old->name;
             if ($branch === Config::get('system', 'branch') || isset($commit->branches) && in_array(Config::get('system', 'branch'), $commit->branches)) {
                 chdir(__DIR__ . '/../../../');
                 shell_exec('git pull 2>&1');
                 shell_exec('composer install');
                 return self::successfull($res);
             }
         }
         return $res->withStatus(204);

     }
 }
