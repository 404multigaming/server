<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Controller\System;

use Slim\Http\Request;
use Slim\Http\Response;
use Server\Controller\Controller;
use Server\Cache\Cache;
use Config\Config;
use Server\Models\Exceptions\RateLimitException;

 /**
  * Class RequestController
  * @package Server\Controller\System
  */
 class RequestController extends Controller {

    /**
    * @param Request $req
    * @param Response $res
    *
    * @return string
    */
    public function isCached(Request $req, Response $res) {
        $ip = self::ip($req);
        $route = $req->getUri()->getPath();
        $body = $req->getBody()->__toString();
        $entry = Cache::get("call-{$ip}-{$route}-{$body}");
        if($entry !== false) {
            return $res->write($entry);
        }
        return null;
    }

     /**
      * @param Request $req
      * @param Response $res
      * @param string $cacheLevel
      */
    public function cacheCall(Request $req, Response $res, string $cacheLevel = 'A') {
        $ip = self::ip($req);
        $route = $req->getUri()->getPath();
        $body = $req->getBody()->__toString();
        $content = $res->getBody()->__toString();
        Cache::set("call-{$ip}-{$route}-{$body}", $content, Config::get('system', "cachelevel{$cacheLevel}"));
    }

     /**
      * @param Request $req
      * @return string
      * @throws RateLimitException
      */
     public function rateLimit(Request $req) {
         $ip = self::ip($req);
         $entry = Cache::get("rateLimit-{$ip}");
         $interval = Config::get('system', 'rateinterval');
         $limit = Config::get('system', 'ratelimit');
         $amount = 0;
         if($entry !== false) {
             $amount = Cache::get("rateLimit-{$ip}");
             if($amount > $limit) {
                 throw new RateLimitException;
             }
             Cache::increment("rateLimit-{$ip}");
         } else {
             Cache::set("rateLimit-{$ip}", 1, $interval);
         }
         return "{$amount}:{$limit}:{$interval}";

     }

     /**
      * @param Request $req
      * @return string
      */
     public function ip(Request $req) : string {
         return $req->getServerParam('REMOTE_ADDR');
     }
 }
