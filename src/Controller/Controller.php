<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Controller;

use Slim\Http\Response;
use Slim\Http\Request;
use Server\Models\Exceptions\InvalidBodyException;
use Server\Models\Exceptions\UnauthorizedException;
use Server\Library\System\SecurityLibrary;
use Server\Cache\Cache;
 /**
  * Class Controller
  * @package Server\Controller
  */
 class Controller {

     /**
      * @param Response     $res
      * @param array|null   $json
      *
      * @return Response
      */
     protected function successfull(Response $res, array $json = null) {
         if($json === null) {
             return $res->withStatus(200);
         }
         return $res->withStatus(200)->withJson($json);
     }

     /**
      * @param Request $req
      *
      * @return object
      * @throws UnauthorizedException
      */
     protected function auth(Request $req) {
         $token = $req->getHeader('Auth');
         if($token === []) {
             throw new UnauthorizedException;
         }
         $token = $token[0];
         $result = SecurityLibrary::decodeToken($req, $token);
         $username = $result->username;
         $user = Cache::get("user.{$username}.login");
         if($user === false) {
             throw new UnauthorizedException;
         }
         return unserialize($user);
     }

     /**
      * @param Request $req
      * @param string $key
      * @param string $defaultValue
      *
      * @return string|int
      * @throws InvalidBodyException;
      */
     protected function post(Request $req, string $key, string $defaultValue = null) {
         $parameter = $req->getParsedBodyParam($key, $defaultValue);
         if($parameter === null) {
             throw new InvalidBodyException;
         }
         return $parameter;
     }


     /**
      * @param Request $req
      * @param string $key
      *
      * @return string|int
      * @throws InvalidBodyException ;
      */
     protected function header(Request $req, string $key) {
       $parameter = $req->getHeader($key);
       if($parameter === null) {
           throw new InvalidBodyException;
       }
       return $parameter[0];
     }
}
