<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Controller\Account;

use Slim\Http\Request;
use Slim\Http\Response;
use Server\Controller\Controller;
use Server\Cache\Cache;
use Server\Models\Exceptions\BaseException;
use Server\Models\Database\Account\User;
use Logging\AccountLogger;
use Server\Library\System\SecurityLibrary;

 /**
  * Class AuthenticationController
  * @package Server\Controller\Account
  */
 class AuthenticationController extends Controller {

     /**
      * @param Request $req
      * @param Response $res
      *
      * @return Response
      * @throws BaseException
      * @throws \Server\Models\Exceptions\InvalidBodyException
      */
     public function login(Request $req, Response $res) {
         $username = self::post($req, 'username');
         $password = self::post($req, 'password');
         $password = SecurityLibrary::hashPassword($password, $username);
         $user = User::findByLogin($username, $password);
         if(!$user) {
             AccountLogger::invalidLogin($req, $username);
             throw new BaseException('Username or password invalid', 403, 'invalidLogin');
         }
         $token = SecurityLibrary::generateToken($req, $username);
         $result['token'] = $token;
         $result['expire'] = time()+1800;
         Cache::set("user.{$username}.login", serialize($user), 1800);
         return self::successfull($res, $result);
     }
 }
