<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Controller\Account;

use Slim\Http\Request;
use Slim\Http\Response;
use Server\Controller\Controller;
use Server\Cache\Cache;
use Server\Models\Exceptions\BaseException;
use Server\Models\Database\Account\User;
use Logging\AccountLogger;
use Server\Library\System\SecurityLibrary;

 /**
  * Class AccountController
  * @package Server\Controller\Account
  */
 class AccountController extends Controller {

     /**
      * @param Request $req
      * @param Response $res
      *
      * @return Response
      * @throws BaseException
      * @throws \Server\Models\Exceptions\InvalidBodyException
      */
     public function create(Request $req, Response $res) {
         $email = self::post($req, 'email');
         $password = self::post($req, 'password');
         $username = self::post($req, 'username');
         $firstname = self::post($req, 'firstname');
         $lastname = self::post($req, 'lastname', '');
         $birthday = self::post($req, 'birthday');

         //Is realistic email and valid dns?
         if(!User::validateEmail($email)) {
             throw new BaseException('The given email is already in use or invalid.', 400, 'invalidEmail');
         }

         //Is Username larger then two chars?
         if(!User::validateUsername($username)) {
             throw new BaseException('The given username is already in use or invalid.', 400, 'invalidUsername');
         }

         //Is password strength good?
         if(!User::validatePassword($password)) {
             throw new BaseException('The given password is invalid.', 400, 'invalidPassword');
         }

         //Is Birthday a realistic unix timestamp?
         if(!User::validateBirthday($birthday)) {
             throw new BaseException('The given birthday is invalid. Please make sure that it is in unix format.', 400, 'invalidBirthday');
         }

         //Hash plainText password into hashed password
         $password = SecurityLibrary::hashPassword($password, $username);

         //Write user in database
         $user = User::create($username, $email, $password, $firstname, $lastname, $birthday);
         AccountLogger::registration($req, $user);
         return $res->withStatus(201);
     }

     /**
      * @param Request $req
      * @param Response $res
      *
      * @return Response
      * @throws \Server\Models\Exceptions\UnauthorizedException
      */
     public function current(Request $req, Response $res) {
         $user = self::auth($req);
         return self::successfull($res, $user->toArray());
     }

     /**
      * @param Request $req
      * @param Response $res
      * @return Response
      * @throws \Server\Models\Exceptions\UnauthorizedException
      */
     public function delete(Request $req, Response $res) {
         $user = self::auth($req);
         User::deleteUser($user);
         Cache::delete("user.{$user->username}.login");
         return $res->withStatus(200);
     }
 }
