<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Cache;

use \Memcached;
use Config\Config;

 /**
  * Class Cache
  * @package Server\Cache
  */
 class Cache {

     /**
      * @var Memcached
      */
     private $server;

     /**
      * @var Cache
      */
     private static $instance;

     /**
      * Cache constructor.
      */
     public function __construct() {
         $this->server = new Memcached();
         $this->server->addServer(Config::get('memcached', 'server'), Config::get('memcached', 'port'));
     }

     /**
      * @param string $key
      *
      * @return string $value
      */
     public function get(string $key) {
         if(self::$instance === null) {
             self::$instance = new Cache;
         }
         return self::$instance->server->get($key);
     }

     /**
      * @param string $key
      * @param string $value
      * @param int $seconds
      */
     public function set(string $key, string $value, int $seconds = 0) {
         if(self::$instance === null) {
             self::$instance = new Cache;
         }
         self::$instance->server->set($key, $value, $seconds);
     }

     public function delete(string $key) {
         if(self::$instance === null) {
             self::$instance = new Cache;
         }
         self::$instance->server->delete($key);
     }


     /**
      * @param string $key
      * @param int $seconds
      */
     public function touch(string $key, int $seconds = 0) {
         if(self::$instance === null) {
             self::$instance = new Cache;
         }
         self::$instance->server->touch($key, $seconds);
     }

      /**
       * @param string $key
       */
      public function increment(string $key) {
          if(self::$instance === null) {
              self::$instance = new Cache;
          }
          self::$instance->server->increment($key);
      }
 }
