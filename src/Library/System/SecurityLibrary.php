<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Library\System;

use Config\Config;
use Slim\Http\Request;
use \Firebase\JWT\JWT;
 /**
  * Class SecurityLibrary
  * @package Server\Library\System
  */
 class SecurityLibrary {

     /**
      * @param string $password
      * @param string $username
      *
      * @return string
      */
     public function hashPassword(string $password, string $username) : string {
            $salt = Config::get('system', 'hashSalt');
            $hash = hash('sha256', "{$username}{$password}{$salt}");
            for ($i=0; $i<=(strlen($username)); $i++) {
                $hash = hash('sha256', "{$salt}$hash");
            }

            return $hash;
     }

     /**
      * @param Request $req
      * @param string $username
      * @return string
      */
     public function generateToken(Request $req, string $username) : string {
         $payload = [
             'ip' => $req->getServerParam('REMOTE_ADDR'),
             'username' => $username,
             'created' => time(),
             'expire' => (time() + 1800)
         ];
         $key = Config::get('system', 'tokenKey');
         return JWT::encode($payload, base64_decode(strtr($key, '-_', '+/')), 'HS256');
     }

     /**
      * @param Request $req
      * @param string $token
      *
      * @return object
      */
     public function decodeToken(Request $req, string $token) : object {
         $key = Config::get('system', 'tokenKey');
         $token = JWT::decode($token, base64_decode(strtr($key, '-_', '+/')), ['HS256']);
         if($token->ip !== $req->getServerParam('REMOTE_ADDR')) {
             return null;
         }
         return $token;
     }

 }
