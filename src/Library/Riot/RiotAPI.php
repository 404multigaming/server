<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Library\Riot;

use Config\Config;
use RiotAPI\LeagueAPI\LeagueAPI;
use RiotAPI\LeagueAPI\Definitions\Region;

/**
  * Class RiotAPI
  * @package Server\Library\Riot
  */
 class RiotAPI {

     /**
      * @return LeagueAPI
      * @throws \RiotAPI\LeagueAPI\Exceptions\GeneralException
      * @throws \RiotAPI\LeagueAPI\Exceptions\SettingsException
      */
    public function api() {
        $api = new LeagueAPI([
	        LeagueAPI::SET_KEY                      => Config::get('riot', 'token'),
	        LeagueAPI::SET_REGION                   => Region::EUROPE_WEST,
            LeagueAPI::SET_CACHE_PROVIDER           => LeagueAPI::CACHE_PROVIDER_MEMCACHED,
            LeagueAPI::SET_CACHE_PROVIDER_PARAMS    => array(array(array(Config::get('memcached', 'server'), Config::get('memcached', 'port')))),
            LeagueAPI::SET_CACHE_CALLS              => true,
            LeagueAPI::SET_CACHE_RATELIMIT          => true,
            LeagueAPI::SET_CALLBACKS_AFTER => function($api, $url, $requestHash, $curlResource) {
                $time = time();
                $message = "**URL:** {$url}\n**TIME:** {$time}";
        		\Logging\BaseLogger::log($message, 'RIOT REQUEST', Config::get('discordChannel', 'riot'), \Logging\BaseLogger::INFO);
        	}
        ]);
        return $api;
    }
 }
