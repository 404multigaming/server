<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Models\Database\Account;

use \Illuminate\Database\Eloquent\Model;

 /**
  * Class Group
  * @package Server\Models\Database\Account
  */
 class Group extends Model {

    /**
     * @var string
     */
    protected $table = 'userGroup';

    /**
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * @param int $id
     *
     * @return Group|null
     */
    public function findById(int $id) {
        return $this->find($id);
    }

    /**
     * @param string $name
     *
     * @return Group|null
     */
    public function findByName(string $name) {
        return $this->where('name', $name)->first();
    }

    /**
     * @param string $tag
     *
     * @return Group|null
     */
    public function findByTag(string $tag) {
        return $this->where('tag', $tag)->first();
    }

    /**
     * @param string $name
     */
    public function setName(string $name) {
        $this->update(['name' => $name]);
    }

    /**
     * @param string $color
     */
    public function setColor(string $color) {
        $this->update(['color' => $color]);
    }

    /**
     * @param string $tag
     */
    public function setTag(string $tag) {
        $this->update(['tag' => $tag]);
    }

    /**
     * @param string $name
     * @param string $color
     * @param string $tag
     *
     * @return boolean
     */
    public function create(string $name, string $color, string $tag) {
        $this->name = $name;
        $this->color = $color;
        $this->tag = $tag;
        return $this->save();
    }
}
