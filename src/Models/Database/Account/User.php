<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Models\Database\Account;

use \Illuminate\Database\Eloquent\Model;

/**
  * Class User
  * @package Server\Models\Database\Account
  */
 class User extends Model {

    /**
     * @var string
     */
    protected $table = 'user';

    /**
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * @param int $id
     *
     * @return User|null
     */
    public function findById(int $id) {
        return self::find($id);
    }

    /**
     * @param string $username
     *
     * @return User|null
     */
    public function findByUsername(string $username) {
        return self::where('username', $username)->first();
    }

    /**
     * @param string $email
     *
     * @return User|null
     */
    public function findByEmail(string $email) {
        return self::where('email', $email)->first();
    }

     /**
      * @param string $username
      * @param string $password
      *
      * @return User|null
      */
    public function findByLogin(string $username, string $password) {
        return self::where('username', $username)->where('password', $password)->first();
    }

     /**
      * @param string $email
      *
      * @return bool
      */
    public function validateEmail(string $email) : bool {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return !self::where('email', $email)->exists();
    }

    /**
     * @param string $username
     *
     * @return bool
     */
    public function validateUsername(string $username) : bool {
        if(strlen($username) <= 3) {
            return false;
        }
        return !self::where('username', $username)->exists();
    }

    /**
     * @param string $password
     *
     * @return bool
     */
    public function validatePassword(string $password) : bool {
        if (strlen($password) < 8) {
            return false;
        }
        if (!preg_match("#[0-9]+#", $password)) {
            return false;
        }
        if (!preg_match("#[a-zA-Z]+#", $password)) {
            return false;
        }
        return true;
    }

    /**
     * @param int $birthday
     *
     * @return bool
     */
    public function validateBirthday(int $birthday) : bool {
        return ($birthday <= PHP_INT_MAX) && ($birthday >= ~PHP_INT_MAX);
    }

    /**
     * Ban the current user
     */
    public function ban() {
        self::update(['banned' => true]);
    }

    /**
     * Unban the current user
     */
    public function unban() {
        self::update(['banned' => false]);
    }

    /**
     * Activate the current user
     */
    public function activate() {
        self::update(['active' => true]);
    }

    /**
     * Deactivate the current user
     */
    public function deactivate() {
        self::update(['deactivate' => true]);
    }

    /**
     * @param Group $group
     */
    public function setGroup(Group $group) {
        self::update(['group' => $group->id]);
    }

     /**
      * @param \Server\Models\Database\Account\Group $groupId
      */
    public function setGroupId(Group $groupId) {
        self::update(['group' => $groupId]);
    }

    /**
     * @param int $birthday
     */
    public function setBirthday(int $birthday) {
        self::update(['birthday' => $birthday]);
    }

    /**
     * @param string $lastname
     */
    public function setLastname(string $lastname) {
        $lastname = ($lastname === '') ? null : $lastname;
        self::update(['lastname' => $lastname]);
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname) {
        self::update(['firstname' => $firstname]);
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password) {
        self::update(['password' => $password]);
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email) {
        self::update(['email' => $email]);
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username) {
        self::update(['username' => $username]);
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $password
     * @param string $firstname
     * @param string $lastname
     * @param int $birthday
     *
     * @return User
     */
    public function create(string $username, string $email, string $password, string $firstname, string $lastname, int $birthday) : User {
        $entity = new User;
        $entity->username = $username;
        $entity->email = $email;
        $entity->password = $password;
        $entity->firstname = $firstname;
        $entity->lastname = $lastname;
        $entity->birthday = $birthday;
        $entity->active = false;
        $entity->banned = false;
        $entity->groupId = 0;
        $entity->save();
        return $entity;
    }
    /**
     * @param User $user
     */
    public function deleteUser(User $user) {
        self::where('id', $user->id)->delete();
    }
}
