<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Models\Exceptions;

 /**
  * Class BaseException
  * @package Server\Models\Exceptions
  */
 class BaseException extends \Exception {

     /**
      * @var string
      */
     protected $type;

     /**
      * @param string $message
      * @param int $code
      * @param string $type
      */
     public function __construct(string $message, int $code, string $type) {
         $this->type = $type;
         parent::__construct($message, $code);
     }

     /**
      * @return array
      */
     public function getResponse() {
         return ['message' => $this->getMessage(), 'type' => $this->type];
     }
 }
