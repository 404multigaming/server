<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

namespace Server\Models\Exceptions;

/**
  * Class InvalidBodyException
  * @package Server\Models\Exceptions
  */
class InvalidBodyException extends BaseException {

    /**
     * RateLimitException Constructor
     */
    public function __construct() {
        parent::__construct('Required body parameter is missing.', 400, 'missingParameter');
    }

}
