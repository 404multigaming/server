<?php
/**
 * LICENSE: This Software is the property of Tim Jambor
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @copyright 2019 Tim Jambor
 * @author    Tim Jambor
 * @link      www.timjambor.com
 */

require __DIR__ . '/../app/app.php';

/*
|--------------------------------------------------------------------------
| Initialize the Application
|--------------------------------------------------------------------------
|
| Once the application is initialize, the routing handler can do his work.
|
*/

$Application->run();
